package com.yubest.demo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author hweiyu
 * @Description
 * @Date 2021/3/1 14:01
 */
@Data
@Accessors(chain = true)
public class DemoRespDTO implements Serializable {

    private static final long serialVersionUID = 1019466745376831818L;

    private Integer c;

    private String d;

}
