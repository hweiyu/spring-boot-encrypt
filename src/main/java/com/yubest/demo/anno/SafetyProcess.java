package com.yubest.demo.anno;

import java.lang.annotation.*;

/**
 * @Author hweiyu
 * @Description
 * @Date 2021/3/10 17:04
 */

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface SafetyProcess {

    /**
     * 请求参数是否解密
     */
    boolean decode() default true;

    /**
     * 响应结果是否加密
     */
    boolean encode() default true;
}