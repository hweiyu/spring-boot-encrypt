package com.yubest.demo.controller;

import cn.hutool.crypto.SecureUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yubest.demo.anno.SafetyProcess;
import com.yubest.demo.dto.DemoReqDTO;
import com.yubest.demo.dto.DemoRespDTO;
import com.yubest.demo.dto.Response;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * @Author hweiyu
 * @Description
 * @Date 2021/3/1 14:01
 */
@RestController
public class DemoController {

    /**
     * 请求参数解密，响应结果加密
     * @param reqDTO
     * @return
     */
    @SafetyProcess
    @PostMapping(value = "/test")
    public Response<DemoRespDTO> test(@RequestBody DemoReqDTO reqDTO) {
        DemoRespDTO respDTO = new DemoRespDTO().setC(3).setD("4");
        return Response.success(respDTO);
    }

    /**
     * 请求参数解密
     * @param reqDTO
     * @return
     */
    @SafetyProcess(encode = false)
    @PostMapping(value = "/test2")
    public Response<DemoRespDTO> test2(@RequestBody DemoReqDTO reqDTO) {
        DemoRespDTO respDTO = new DemoRespDTO().setC(3).setD("4");
        return Response.success(respDTO);
    }

    /**
     * 响应结果加密
     * @param reqDTO
     * @return
     */
    @SafetyProcess(decode = false)
    @PostMapping(value = "/test3")
    public Response<DemoRespDTO> test3(@RequestBody DemoReqDTO reqDTO) {
        DemoRespDTO respDTO = new DemoRespDTO().setC(3).setD("4");
        return Response.success(respDTO);
    }

    /**
     * 不进行加解密
     * @param reqDTO
     * @return
     */
    @SafetyProcess(decode = false, encode = false)
    @PostMapping(value = "/test4")
    public Response<DemoRespDTO> test4(@RequestBody DemoReqDTO reqDTO) {
        DemoRespDTO respDTO = new DemoRespDTO().setC(3).setD("4");
        return Response.success(respDTO);
    }

    public static void main(String[] args) throws JsonProcessingException {
        DemoReqDTO reqDTO = new DemoReqDTO().setA(1).setB("2");
        System.out.println("源字符串：" + new ObjectMapper().writeValueAsString(reqDTO));
        //加密
        String str = encode(reqDTO);
        System.out.println("加密后：" + str);

        //解密
        String res = decode(str);
        System.out.println("解密后：" + res);
    }

    private static String encode(Object obj) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String bodyStr = objectMapper.writeValueAsString(obj);
            //aes加密
            byte[] bytes = SecureUtil.aes("1234567890abcdef".getBytes()).encrypt(bodyStr);
            //base64编码返回
            return Base64.getEncoder().encodeToString(bytes);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String decode(String str) {
        byte[] bytes = Base64.getDecoder().decode(str);
        //aes解密
        byte[] body = SecureUtil.aes("1234567890abcdef".getBytes()).decrypt(bytes);
        return new String(body, StandardCharsets.UTF_8);
    }
}
